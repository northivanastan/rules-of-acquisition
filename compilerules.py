#!/usr/bin/python3
# This script converts the rules.txt file to HTML format.
# It should never be executed on the server.
rulesFile=open('rules.txt','r')
rulesList=rulesFile.readlines()
ruleno=1
for rule in rulesList:
    if 'unknown_rule' in rule:
        print('<div class="rule rule-color-'+str(ruleno%2)+'" title="In other words, this/these rule(s) only exist in fanfiction.">'+
        "Some rules are only available through a Nagal Library Online subscription! <a href=#>Subscribe now!</a></div>")
    else:
        print('<div class="rule rule-color-'+str(ruleno%2)+'">'+rule+'</div>')
    ruleno+=1
